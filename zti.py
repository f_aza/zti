import spotlight
import pprint
import argparse
import sys
import os
import configparser

COMMANDS = []


def parse_args():
    def parse_error(message):
        errmsg = '{0}: error: {1}\n'.format(SCRIPT, message)
        sys.stderr.write(errmsg)
        parser.print_help()
        sys.exit(1)

    SCRIPT = os.path.basename(sys.argv[0])
    parser = argparse.ArgumentParser(prog=SCRIPT,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description='ZTI projekt',
                                     epilog='Autorzy: Rafal Jendraszak, Jerzy Pilarczyk')

    parser.add_argument('--file-path', default='data.txt',
                        help='Path to file with text to process')
    parser.add_argument('--config', default='config.ini',
                        help='Path to configuration file')

    args = vars(parser.parse_args())

    # print(args)

    return args


def parse_config(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    return dict(config['FILTER'])


def parse_data(data_file):
    text = ""
    with open(data_file, 'r') as file:
        for line in file.readlines():
            text += line

    return text


def main():
    args = parse_args()

    # Process text file
    text_to_process = parse_data(args['file_path'])
    # print(text_to_process)


    # Process config file
    filters = parse_config(args['config'])
    # print(filter)

    # Run annotations
    annotaions = spotlight.annotate('http://model.dbpedia-spotlight.org/en/annotate',
                                    text_to_process,
                                    filters=filters)
    pprint.pprint(annotaions)

if __name__ == '__main__':
     main()
